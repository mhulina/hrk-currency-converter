package com.currencyconverter.entity;

import com.currencyconverter.utility.Database;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

@Entity
@Table(name = "currency_download_tbl")
public class Currency {

    public static Database database = new Database();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "download_date")
    private String downloadDate;

    @SerializedName("Dr\u017eava")
    @Column(name = "country")
    private String currencyCountry;

    @SerializedName("Valuta")
    @Column(name = "name")
    private String currencyName;

    @SerializedName("\u0160ifra valute")
    @Column(name = "code")
    private int currencyCode;

    @SerializedName("Kupovni za devize")
    @Column(name = "buy_rate")
    private String buyRate;

    @SerializedName("Prodajni za devize")
    @Column(name = "central_rate")
    private String sellRate;

    @SerializedName("Srednji za devize")
    @Column(name = "sell_rate")
    private String centralRate;

    @SerializedName("Jedinica")
    @Column(name = "unit")
    private int currencyUnit;

    @SerializedName("Datum primjene")
    @Column(name = "currency_date")
    private String currencyDate;

    @SerializedName("Broj te\u010Dajnice")
    @Column(name = "exchange_rate")
    private int exchangeRate;

    public Currency() {
    }

    public String getCurrencyCountry() {
        return currencyCountry;
    }

    public void setCurrencyCountry(String currencyCountry) {
        this.currencyCountry = currencyCountry;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public int getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(int currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBuyRate() {
        return buyRate;
    }

    public void setBuyRate(String buyRate) {
        this.buyRate = buyRate;
    }

    public String getSellRate() {
        return sellRate;
    }

    public void setSellRate(String sellRate) {
        this.sellRate = sellRate;
    }

    public String getCentralRate() {
        return centralRate;
    }

    public void setCentralRate(String centralRate) {
        this.centralRate = centralRate;
    }

    public int getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(int currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public String getCurrencyDate() {
        return currencyDate;
    }

    public void setCurrencyDate(String currencyDate) {
        this.currencyDate = currencyDate;
    }

    public int getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(int exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(String downloadDate) {
        this.downloadDate = downloadDate;
    }

    @Override
    public String toString() {
        return "\nCurrency{" +
                "\ncurrencyCountry='" + currencyCountry + '\'' +
                ", \ncurrencyName='" + currencyName + '\'' +
                ", \ncurrencyCode=" + currencyCode +
                ", \nbuyRate='" + buyRate + '\'' +
                ", \nsellRate='" + sellRate + '\'' +
                ", \ncentralRate='" + centralRate + '\'' +
                ", \ncurrencyUnit=" + currencyUnit +
                ", \ncurrencyDate='" + currencyDate + '\'' +
                ", \nexchangeRate=" + exchangeRate +
                '}';
    }
}