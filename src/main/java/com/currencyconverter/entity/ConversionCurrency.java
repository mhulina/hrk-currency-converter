package com.currencyconverter.entity;

import com.currencyconverter.utility.HnbApi;
import org.hibernate.Session;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


public class ConversionCurrency {

    @Pattern(regexp = "^[0-9]+(\\.[0-9]{1,2})?$", message = "Must be digits only and in a format XX.XX")
    private BigDecimal amountFromCurrency = new BigDecimal(0.00);

    private BigDecimal convertedAmount = new BigDecimal(0.00);

    private String fromCurrencyName;
    private String toCurrencyName;
    @DateTimeFormat(pattern = "YYYY-MM-DD")
    private String currencyDate;
    private String conversionType;

    private List<Currency> currencyList = new ArrayList<>();

    public ConversionCurrency() {
        Session session = Currency.database.connect(Currency.class);

        getHnbCurrencies(session);

        session.close();
    }

    public void getHnbCurrencies(Session session) {
        try {
            currencyDate = reformatDate(currencyDate);

           // Session session = Currency.database.connect(Currency.class);
            currencyList = session.createQuery("from Currency where downloadDate='" + LocalDate.now() +
                                                "' AND currencyDate='" + currencyDate + "'").getResultList();

            currencyDate = reformatDate(currencyDate);

            if(0 == currencyList.size()){

                HnbApi hnbApi = new HnbApi();

                hnbApi.connect(currencyDate);
                hnbApi.getJsonString();

                currencyList = hnbApi.parseJsonStringToGetList();

                for (Currency currency: currencyList) {
                    session.beginTransaction();
                    currency.setDownloadDate(LocalDate.now().toString());
                    session.save(currency);
                    session.getTransaction().commit();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String reformatDate(String tmpCurrencyDate){
        DateTimeFormatter formats = new DateTimeFormatterBuilder()
                                        .appendOptional(DateTimeFormatter.ofPattern("uuuu-MM-dd"))
                                        .appendOptional(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
                                        .toFormatter();

        if(null == tmpCurrencyDate){
            tmpCurrencyDate = LocalDate.now().toString();
        }

        if(LocalDate.parse(tmpCurrencyDate, formats).toString()
                .equals(tmpCurrencyDate)){
            tmpCurrencyDate = LocalDate.parse(tmpCurrencyDate, formats)
                                        .format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        }else{
            tmpCurrencyDate = LocalDate.parse(tmpCurrencyDate, formats)
                                        .format(DateTimeFormatter.ofPattern("uuuu-MM-dd"));
        }

        return tmpCurrencyDate;
    }

    public BigDecimal getAmountFromCurrency() {
        return amountFromCurrency;
    }

    public void setAmountFromCurrency(BigDecimal amountFromCurrency) {
        this.amountFromCurrency = amountFromCurrency;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<Currency> currencyList) {
        this.currencyList = currencyList;
    }

    public List<String> valutaList(){
        List<String> list = new ArrayList<>();

        for (Currency valuta : currencyList) {
            if(!valuta.getCurrencyName().equals("XDR")){
                list.add(valuta.getCurrencyName());
            }
        }

        list.add("HRK");
        List<String> listSet = new ArrayList<>(new HashSet<>(list));

        return listSet;
    }

    public String getFromCurrencyName() {
        return fromCurrencyName;
    }

    public void setFromCurrencyName(String fromCurrencyName) {
        this.fromCurrencyName = fromCurrencyName;
    }

    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(BigDecimal convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    public String getCurrencyDate() {
        return currencyDate;
    }

    public void setCurrencyDate(String currencyDate) {
        this.currencyDate = currencyDate;
    }

    public String getConversionType() {
        return conversionType;
    }

    public void setConversionType(String conversionType) {
        this.conversionType = conversionType;
    }

    public String getToCurrencyName() {
        return toCurrencyName;
    }

    public void setToCurrencyName(String toCurrencyName) {
        this.toCurrencyName = toCurrencyName;
    }

    @Override
    public String toString() {
        return "ConversionCurrency{" +
                "\namountFromCurrency=" + amountFromCurrency +
                ", \nconvertedAmount=" + convertedAmount +
                ", \nfromCurrencyName='" + fromCurrencyName + '\'' +
                ", \ntoCurrencyName='" + toCurrencyName + '\'' +
                ", \ncurrencyDate='" + currencyDate + '\'' +
                ", \nconversionType='" + conversionType + '\'' +
                '}';
    }
}