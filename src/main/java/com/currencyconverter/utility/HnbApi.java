package com.currencyconverter.utility;

import com.currencyconverter.entity.Currency;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class HnbApi {

    private static String API_HOST = "https://api.hnb.hr/tecajn/v1";
    private static String API_DATE_SEARCH = "?datum=";

    private HttpURLConnection connection;
    private URL url;
    private String jsonString;

    public HnbApi(){
    }

    public void connect(String currencyDate){
        try {
            if(null != currencyDate){
                url = new URL(API_HOST + API_DATE_SEARCH + currencyDate);
            }
            else{
                url = new URL(API_HOST);
            }

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            // check connection
            int responseCode = connection.getResponseCode();

            if (responseCode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responseCode);
            }

        }catch (Exception e){
            System.out.println("Unable to connect to API.");
            e.printStackTrace();
        }
    }


    public String getJsonString(){
        jsonString = new String();

        try {
            Scanner scanner = new Scanner(url.openStream());
            scanner.useLocale(Locale.getDefault());

            while(scanner.hasNext()){
                jsonString += scanner.nextLine();
            }

            scanner.close();

        } catch (Exception e){
            System.out.println("Could not openStream to get Json data.");
            e.printStackTrace();
        }

        return jsonString;
    }

    public List<Currency> parseJsonStringToGetList(){
        List<Currency> currencyList = new ArrayList<>();

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonElements = (JsonArray) jsonParser.parse(jsonString);

        Gson gson = new Gson();
        for(JsonElement jsonElement : jsonElements){
            Currency tmpCurrency = gson.fromJson(jsonElement, Currency.class);

            currencyList.add(tmpCurrency);
        }

        return currencyList;
    }


}
