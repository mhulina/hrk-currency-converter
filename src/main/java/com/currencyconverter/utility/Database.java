package com.currencyconverter.utility;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public class Database {

    private static SessionFactory sessionFactory;
    private static Session mainSession;

    public Database(){

    }

    public void closeFactory(){
        sessionFactory.close();
    }

    public void closeSession(){
        mainSession.close();
    }

    public Session connect(Class EntityClass){
        try{
            mainSession = connectToDatabase(EntityClass);
            checkConnection(sessionFactory, mainSession);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mainSession;
    }

    public Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    private static void checkConnection(SessionFactory sessionFactory, Session session){
        System.out.println("Connecting to DB: " + sessionFactory.getProperties().get("connection.url") + ", on: "
                + System.getProperty("os.name"));

        if (session.isConnected()){
            System.out.println("Connection successful.");
        }else{
            System.out.println("Connection unsuccessful.");
        }
    }

    private static Session connectToDatabase(Class EntityClass){
        if("Linux".equals(System.getProperty("os.name"))){
            sessionFactory = new Configuration().configure(new File("hibernate-linux.cfg.xml"))
                    .addAnnotatedClass(EntityClass).buildSessionFactory();
        }else{
            sessionFactory = new Configuration().configure(new File("hibernate-windows.cfg.xml"))
                    .addAnnotatedClass(EntityClass).buildSessionFactory();
        }

        return sessionFactory.openSession();
    }
}
