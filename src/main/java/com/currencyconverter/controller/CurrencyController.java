package com.currencyconverter.controller;

import com.currencyconverter.entity.ConversionCurrency;
import com.currencyconverter.entity.Currency;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


@Controller
@RequestMapping
public class CurrencyController {

    @RequestMapping("/")
    public String homeForm(Model model){
        Locale.setDefault(Locale.forLanguageTag("hr-HR"));

        System.out.println(Locale.getDefault().toString());

        // dodaj Currency objekt u model
        model.addAttribute("conversionCurrency", new ConversionCurrency());

        return "homepage";
    }

    @RequestMapping("/processForm")
    public String processForm(@Valid @ModelAttribute("conversionCurrency") ConversionCurrency currency,
                              BindingResult bindingResult){

        Session session = Currency.database.connect(Currency.class);

        currency.getHnbCurrencies(session);

        currency.setConvertedAmount(convertCurrency(currency));

        // log the input data
        System.out.println(currency.toString());

        System.out.println("Binding result: " + bindingResult + "\n");

        session.close();

        // check if there are errors on form
        if(bindingResult.hasErrors()){
            return "homepage";
        }else{
            return "homepage";
        }
    }

    private BigDecimal convertCurrency(ConversionCurrency conversionCurrency){
        BigDecimal convertedAmount;

        convertedAmount = getCurrenciesForExchange(conversionCurrency);

        return convertedAmount.setScale(2, RoundingMode.HALF_EVEN);
    }

    @NotNull
    private BigDecimal getCurrenciesForExchange(ConversionCurrency conversionCurrency){
        BigDecimal convertedAmount;
        List<Currency> exchangeCurrencies = new ArrayList<>();

        getFromAndToCurrencies(conversionCurrency, exchangeCurrencies);

        convertedAmount = checkCurrencyUnit(conversionCurrency, exchangeCurrencies);

        return convertedAmount;
    }

    private void getFromAndToCurrencies(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        // check FROM which currency you are exchanging and add it to the list
        for (Currency tmpFromCurrency : conversionCurrency.getCurrencyList()) {
            if ("HRK".equals(conversionCurrency.getFromCurrencyName())) {
                Currency currencyHRK = new Currency();
                currencyHRK.setCurrencyName("HRK");

                exchangeCurrencies.add(currencyHRK);
                break;
            }
            if (tmpFromCurrency.getCurrencyName().equals(conversionCurrency.getFromCurrencyName())) {
                exchangeCurrencies.add(tmpFromCurrency);

                break;
            }
        }
        //check TO which currency you are exchanging and add it to the list
        for (Currency tmpToCurrency : conversionCurrency.getCurrencyList()) {
            if ("HRK".equals(conversionCurrency.getToCurrencyName())) {
                Currency currencyHRK = new Currency();
                currencyHRK.setCurrencyName("HRK");

                exchangeCurrencies.add(currencyHRK);
                break;
            }
            if (tmpToCurrency.getCurrencyName().equals(conversionCurrency.getToCurrencyName())) {
                exchangeCurrencies.add(tmpToCurrency);

                break;
            }
        }
    }

    @NotNull
    private BigDecimal checkCurrencyUnit(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        BigDecimal convertedAmount;
        int cnt = 0;

        // check if both of the currencies have a unit value of 100
        for(Currency tmpCurrency : exchangeCurrencies){
            if(100 == tmpCurrency.getCurrencyUnit()){
                cnt++;
            }
        }

        // if both currencies have a unit of 100 or none have, do not divide
        if(2 == cnt || 1 > cnt){
            convertedAmount = checkForHrk(conversionCurrency, exchangeCurrencies);
        }
        // check which currency has a unit of 100 and calculate accordingly
        else if(100 != exchangeCurrencies.stream().findFirst().get().getCurrencyUnit()){
            convertedAmount = checkForHrk(conversionCurrency, exchangeCurrencies).multiply(new BigDecimal(exchangeCurrencies
                                                                                                            .get(exchangeCurrencies.size() - 1)
                                                                                                            .getCurrencyUnit()));
        }
        else{
            convertedAmount = checkForHrk(conversionCurrency, exchangeCurrencies).divide(new BigDecimal(exchangeCurrencies
                                                                                        .stream()
                                                                                        .findFirst()
                                                                                        .get()
                                                                                        .getCurrencyUnit()));
        }

        return convertedAmount;
    }

    @NotNull
    private BigDecimal checkForHrk(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        BigDecimal convertedAmount;

        // check if any currency is HRK
        if("HRK".equals(conversionCurrency.getToCurrencyName()) || "HRK".equals(conversionCurrency.getFromCurrencyName())){
            convertedAmount = checkConversionTypeAndConvertHRK(conversionCurrency, exchangeCurrencies);
        }
        else{
            convertedAmount = checkForConversionTypeAndConvert(conversionCurrency, exchangeCurrencies);
        }

        return convertedAmount;
    }

    @NotNull
    private BigDecimal checkConversionTypeAndConvertHRK(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        BigDecimal convertedAmount;
        if("HRK".equals(conversionCurrency.getToCurrencyName())){
            if ("B".equals(conversionCurrency.getConversionType())) {
                convertedAmount = convertWithBuyRatesToHrk(conversionCurrency, exchangeCurrencies);
            }
            else if ("C".equals(conversionCurrency.getConversionType())) {
                convertedAmount = convertWithCentralRatesToHRK(conversionCurrency, exchangeCurrencies);
            }
            else {
                convertedAmount = convertWithSellRatesToHRK(conversionCurrency, exchangeCurrencies);
            }
        }
        else{
            if ("B".equals(conversionCurrency.getConversionType())) {
                convertedAmount = convertWithBuyRatesFromHRK(conversionCurrency, exchangeCurrencies);
            }
            else if ("C".equals(conversionCurrency.getConversionType())) {
                convertedAmount = convertWithCentralRatesFromHRK(conversionCurrency, exchangeCurrencies);
            }
            else {
                convertedAmount = convertWithSellRatesFromHRK(conversionCurrency, exchangeCurrencies);
            }
        }
        return convertedAmount;
    }

    @NotNull
    private BigDecimal convertWithSellRatesFromHRK(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return conversionCurrency.getAmountFromCurrency()
                .divide(new BigDecimal(exchangeCurrencies
                        .get(exchangeCurrencies.size() - 1)
                        .getSellRate()
                        .replace(",", ".")), 7, RoundingMode.HALF_EVEN);
    }

    @NotNull
    private BigDecimal convertWithCentralRatesFromHRK(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
            return conversionCurrency.getAmountFromCurrency()
                .divide(new BigDecimal(exchangeCurrencies
                        .get(exchangeCurrencies.size() - 1)
                        .getCentralRate()
                        .replace(",", ".")), 7, RoundingMode.HALF_EVEN);
    }

    @NotNull
    private BigDecimal convertWithBuyRatesFromHRK(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return conversionCurrency.getAmountFromCurrency()
                .divide(new BigDecimal(exchangeCurrencies
                        .get(exchangeCurrencies.size() - 1)
                        .getBuyRate()
                        .replace(",", ".")), 7, RoundingMode.HALF_EVEN);
    }

    @NotNull
    private BigDecimal convertWithBuyRatesToHrk(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return conversionCurrency.getAmountFromCurrency()
                .multiply(new BigDecimal(
                        exchangeCurrencies
                                .stream()
                                .findFirst()
                                .get()
                                .getBuyRate()
                                .replace(",", ".")));
    }

    @NotNull
    private BigDecimal convertWithCentralRatesToHRK(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return conversionCurrency.getAmountFromCurrency()
                .multiply(new BigDecimal(
                        exchangeCurrencies
                                .stream()
                                .findFirst()
                                .get()
                                .getCentralRate()
                                .replace(",", ".")));
    }

    @NotNull
    private BigDecimal convertWithSellRatesToHRK(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return conversionCurrency.getAmountFromCurrency()
                .multiply(new BigDecimal(
                        exchangeCurrencies
                                .stream()
                                .findFirst()
                                .get()
                                .getSellRate()
                                .replace(",", ".")));
    }

    @NotNull
    private BigDecimal checkForConversionTypeAndConvert(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        BigDecimal convertedAmount;
        if ("B".equals(conversionCurrency.getConversionType())) {
            convertedAmount = convertWithBuyRates(conversionCurrency, exchangeCurrencies);
        }
        else if ("C".equals(conversionCurrency.getConversionType())) {
            convertedAmount = convertWithCentralRates(conversionCurrency, exchangeCurrencies);
        }
        else {
            convertedAmount = convertWithSellRates(conversionCurrency, exchangeCurrencies);
        }
        return convertedAmount;
    }

    @NotNull
    private BigDecimal convertWithSellRates(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return new BigDecimal(exchangeCurrencies.stream()
                .findFirst()
                .get()
                .getSellRate()
                .replace(",", "."))
                .divide(new BigDecimal(exchangeCurrencies
                        .get(exchangeCurrencies.size() - 1)
                        .getSellRate()
                        .replace(",", ".")), 7, RoundingMode.HALF_EVEN)
                .multiply(conversionCurrency.getAmountFromCurrency());
    }

    @NotNull
    private BigDecimal convertWithCentralRates(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return new BigDecimal(exchangeCurrencies.stream()
                .findFirst()
                .get()
                .getCentralRate()
                .replace(",", "."))
                .divide(new BigDecimal(exchangeCurrencies
                        .get(exchangeCurrencies.size() - 1)
                        .getCentralRate()
                        .replace(",", ".")), 7, RoundingMode.HALF_EVEN)
                .multiply(conversionCurrency.getAmountFromCurrency());
    }

    @NotNull
    private BigDecimal convertWithBuyRates(ConversionCurrency conversionCurrency, List<Currency> exchangeCurrencies) {
        return new BigDecimal(exchangeCurrencies.stream()
                .findFirst()
                .get()
                .getBuyRate()
                .replace(",", "."))
                .divide(new BigDecimal(exchangeCurrencies
                        .get(exchangeCurrencies.size() - 1)
                        .getBuyRate()
                        .replace(",", ".")), 7, RoundingMode.HALF_EVEN)
                .multiply(conversionCurrency.getAmountFromCurrency());
    }

}