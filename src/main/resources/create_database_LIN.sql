/* POSTGRES CREATION
DROP DATABASE IF EXISTS CURRENCY;

CREATE DATABASE CURRENCY WITH ENCODING 'UTF8';


    CREATE TABLE IF NOT EXISTS CURRENCY_DOWNLOAD_TBL(
        id serial PRIMARY KEY,
        download_date VARCHAR (25) NOT NULL,
        country VARCHAR (25),
        name VARCHAR (10) NOT NULL,
        code INT NOT NULL,
        buy_rate VARCHAR (30) NOT NULL,
        central_rate VARCHAR (30) NOT NULL,
        sell_rate VARCHAR (30) NOT NULL,
        unit INT NOT NULL,
        currency_date VARCHAR (25) NOT NULL,
        exchange_rate INT NOT NULL
        );
*/
/* MYSQL CREATION */
CREATE DATABASE IF NOT EXISTS CURRENCY character set = 'utf8';

CREATE TABLE IF NOT EXISTS CURRENCY_DOWNLOAD_TBL(
    id serial PRIMARY KEY,
    download_date VARCHAR (25) NOT NULL,
    country VARCHAR (25),
    name VARCHAR (10) NOT NULL,
    code INT NOT NULL,
    buy_rate VARCHAR (30) NOT NULL,
    central_rate VARCHAR (30) NOT NULL,
    sell_rate VARCHAR (30) NOT NULL,
    unit INT NOT NULL,
    currency_date VARCHAR (25) NOT NULL,
    exchange_rate INT NOT NULL
);
