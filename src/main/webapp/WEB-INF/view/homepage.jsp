<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: fluffy
  Date: 23. 06. 2021.
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Currency Converter</title>
    <style>
        .error{color: red}
        input{text-align: right}
        label{text-align: right}
    </style>
</head>
<body>
<h1>
    HRK Currency converter
</h1>
<br />
<br />
<form:form action="processForm" modelAttribute="conversionCurrency">
    <p>
        <form:input type="date" path="currencyDate" />
    </p>
    <br />
    <p>
        Amount: <form:input path="amountFromCurrency" />
        <form:errors path="amountFromCurrency" cssClass="error" />
        <form:select path="fromCurrencyName">
            <form:options items="${conversionCurrency.valutaList()}" />
        </form:select>
    </p>
    <br />
    <p>
        To which currency you wish to convert:
        <form:select path="toCurrencyName">
            <form:options items="${conversionCurrency.valutaList()}" />
        </form:select>
    </p>
    <br />
    <p>
        Converted amount: <form:input path="convertedAmount" readonly="true"/>
    </p>
    <br />
    <p>
        <form:radiobutton path="conversionType" label="Buy Rate" value="B" />
        <form:radiobutton path="conversionType" label="Central Rate" value="C" />
        <form:radiobutton path="conversionType" label="Sell Rate" value="S" />
    </p>

    <br /><br />

    <input type="submit" value="Convert" />
</form:form>
</body>
</html>